/*
Copyright 2024 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	"reflect"
	"strconv"

	"github.com/go-logr/logr"
	"github.com/pkg/errors"
	hostquotav1alpha1 "gitlab.com/Orange-OpenSource/kanod/host-quota/api/v1alpha1"
	hostv1alpha1 "gitlab.com/Orange-OpenSource/kanod/host-quota/host_api/v1alpha1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logger "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
)

// HostQuotaReconciler reconciles a HostQuota object
type HostQuotaReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

type UsageQuota struct {
	Quota      *hostquotav1alpha1.Quota
	UsageValue int
}

// +kubebuilder:rbac:groups=kanod.io,resources=hostquotas,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=kanod.io,resources=hostquotas/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=kanod.io,resources=hostquotas/finalizers,verbs=update
// +kubebuilder:rbac:groups=kanod.io,resources=hosts,verbs=get;list;watch
func (r *HostQuotaReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := logger.FromContext(ctx)

	hostQuota := &hostquotav1alpha1.HostQuota{}
	if err := r.Client.Get(ctx, req.NamespacedName, hostQuota); err != nil {
		if apierrors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		log.Error(err, "cannot get HostQuota resource", "name", hostQuota.Name, "namespace", hostQuota.Namespace)
		return ctrl.Result{}, err
	}

	currentUsage, err := r.UpdateUsage(ctx, *hostQuota, log)
	if err != nil {
		log.Error(err, "cannot count host resource", "name", hostQuota.Name, "namespace", hostQuota.Namespace)
		return ctrl.Result{}, err
	}

	if err = r.updateStatus(req.NamespacedName, hostQuota.Spec.Hard, currentUsage, log); err != nil {
		log.Error(err, "Failed to update status ")
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *HostQuotaReconciler) UpdateUsage(
	ctx context.Context,
	hostQuota hostquotav1alpha1.HostQuota,
	log logr.Logger,
) (*[]UsageQuota, error) {
	updatedUsageQuota := []UsageQuota{}
	hostList := &hostv1alpha1.HostList{}

	options := client.ListOptions{
		Namespace: hostQuota.Namespace,
	}

	err := r.Client.List(context.Background(), hostList, &options)
	if err != nil {
		if apierrors.IsNotFound(err) {
			log.Info("no host in namespace", "namespace", hostQuota.Namespace)
			return nil, nil
		} else {
			log.Error(err, "failed to list host")
			return nil, err
		}
	}

	for _, quota := range hostQuota.Spec.Hard {
		usedCapacity, err := ComputeUsedCapacity(quota, hostList.Items, log)
		if err != nil {
			return nil, err
		}

		usageQuota := UsageQuota{}
		usageQuota.Quota = quota.DeepCopy()
		usageQuota.UsageValue = usedCapacity
		updatedUsageQuota = append(updatedUsageQuota, usageQuota)
	}

	return &updatedUsageQuota, nil
}

func quotaLabelMatchHost(hostLabels map[string]string, quota hostquotav1alpha1.Quota) bool {
	for labelSelector := range quota.LabelSelector {
		hostLabelValue, ok := hostLabels[labelSelector]
		if !ok {
			return false
		}

		if hostLabelValue != quota.LabelSelector[labelSelector] {
			return false
		}
	}

	labelValue, ok := hostLabels[quota.LabelName]
	if !ok {
		return false
	}

	if quota.LabelValue != "" && labelValue != quota.LabelValue {
		return false
	}

	return true
}

// compute current usage for a specific quota label
func ComputeUsedCapacity(
	quota hostquotav1alpha1.Quota,
	hostList []hostv1alpha1.Host,
	log logr.Logger,
) (int, error) {
	usageValue := 0
	for _, host := range hostList {
		hostMatchLabels := host.Spec.HostSelector.MatchLabels
		if hostMatchLabels == nil {
			continue
		}
		if !quotaLabelMatchHost(hostMatchLabels, quota) {
			continue
		}

		if quota.Aggregate {
			val, err := strconv.Atoi(hostMatchLabels[quota.LabelName])
			if err != nil {
				log.Error(err, "error during string conversion to int", "hostMatchLabels", hostMatchLabels, "quota.LabelName", quota.LabelName)
				return 0, err
			}
			usageValue += val
		} else {
			usageValue += 1
		}

	}

	return usageValue, nil
}

func (r *HostQuotaReconciler) updateStatus(
	key types.NamespacedName,
	quotas []hostquotav1alpha1.Quota,
	usageQuota *[]UsageQuota,
	log logr.Logger,
) error {
	ctx := context.Background()
	log.Info("Updating hostQuota status", "NamespacedName", key)

	currentString := ""
	usedQuotaList := []hostquotav1alpha1.Quota{}

	for _, item := range *usageQuota {
		used := item.Quota.DeepCopy()
		used.Quantity = item.UsageValue
		usedQuotaList = append(usedQuotaList, *used)
		labelNameValue := ""
		if item.Quota.LabelValue != "" {
			labelNameValue = fmt.Sprintf("%s:%s", item.Quota.LabelName, item.Quota.LabelValue)
		} else {
			labelNameValue = item.Quota.LabelName
		}
		count := fmt.Sprintf("%s : %d/%d ", labelNameValue, item.UsageValue, item.Quota.Quantity)
		currentString += count
	}

	status := hostquotav1alpha1.HostQuotaStatus{
		Hard:      quotas,
		Used:      usedQuotaList,
		UsedQuota: currentString,
	}

	hostQuota := &hostquotav1alpha1.HostQuota{}

	if err := r.Get(ctx, key, hostQuota); err != nil {
		log.Error(err, "failed to get hostQuota", "hostQuota", hostQuota.Name)
		return err
	}
	if !reflect.DeepEqual(hostQuota.Status, status) {
		hostQuota.Status = status
		err := r.Status().Update(ctx, hostQuota)
		if err != nil {
			log.Error(err, "failed to update hostQuota status", "hostQuota", hostQuota.Name)
			return err
		}
	}

	return nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *HostQuotaReconciler) SetupWithManager(mgr ctrl.Manager) error {
	pred := predicate.GenerationChangedPredicate{}
	return ctrl.NewControllerManagedBy(mgr).
		For(&hostquotav1alpha1.HostQuota{}).
		Watches(
			&hostv1alpha1.Host{},
			handler.EnqueueRequestsFromMapFunc(r.HostToHostQuota),
		).
		WithEventFilter(pred).
		Complete(r)
}

func (r *HostQuotaReconciler) HostToHostQuota(_ context.Context, obj client.Object) []ctrl.Request {
	requests := []ctrl.Request{}
	log := logger.Log.WithName("host-quota-controller")
	if host, ok := obj.(*hostv1alpha1.Host); ok {
		hostQuotaList := &hostquotav1alpha1.HostQuotaList{}
		err := r.Client.List(context.Background(), hostQuotaList, &client.ListOptions{
			Namespace: host.Namespace,
		})
		if err != nil {
			if apierrors.IsNotFound(err) {
				return requests
			} else {
				log.Error(err, "Failed to list HostQuota")
				return requests
			}
		}
		for _, hostQuota := range hostQuotaList.Items {
			req := ctrl.Request{
				NamespacedName: types.NamespacedName{
					Name:      hostQuota.Name,
					Namespace: hostQuota.Namespace,
				},
			}

			requests = append(requests, req)
		}
	} else {
		log.Error(errors.Errorf("expected a Host but got a %T", obj),
			"failed to get HostQuota for Host",
		)
	}
	return requests
}
