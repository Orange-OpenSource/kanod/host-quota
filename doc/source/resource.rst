The HostQuota resource
======================

The ``HostQuota`` custom resource is a new Kubernetes resource that indicates the max number of host (in a namespace) 
containing specific label or label:value in their ``.spec.hostSelector.matchLabels`` field

HostQuota specification
-----------------------

The ``hard`` field in the spec of the ``HostQuota`` resource contains a list of quota. 
A quota is map containing the following fields:

- ``labelName`` field (mandatory) : label name used to filter the host
- ``labelValue`` field (optional) : value of the labelName
- ``labelSelector`` field (optional) : a map of `label:value` used for the host selection for this quota. 
  If all the `label:value` exists in the `.spec.hostSeletor.matchLabels` field of the `host` resource, this host is taken into account for this quota.
- ``quantity`` field (mandatory) : 

  - if ``aggregate`` field is ``false`` : ``quantity`` is the max number of hosts with this labelName or labelName/labelValue
  - if ``aggregate`` field is ``true`` : ``quantity`` is the max value for the sum of the values of the ``labelName`` label (for the hosts associated with this labelName)
- ``aggregate`` field (optional) : if true, the value associated with the host ``labelName`` is accumulated


Here is example of `HostQuota` resource :

.. code-block:: yaml
    
    apiVersion: kanod.io/v1alpha1
    kind: HostQuota
    metadata:
      name: hostquota-cluster1
      namespace: capm3

    spec:
      hard:
      - labelName: host.kanod.io/kind
        labelValue: baremetal
        quantity: 2
      - labelName: host.kanod.io/kind
        labelValue: kubevirt
        quantity: 5
      - labelName: host.kanod.io/cores
        aggregate: true
        quantity: 3
        labelSelector:
          host.kanod.io/kind: kubevirt


HostQuota usage
---------------

The HostQuota resources is used by the ``host-operator`` controller to validate (through webhook) the creation of a new ``host`` resource.
