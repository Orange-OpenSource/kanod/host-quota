/*
Copyright 2024 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Quota defines the max number of host (quota) for a given label or label/value
type Quota struct {
	// LabelSelector contains a map of label/value for host selection
	LabelSelector map[string]string `json:"labelSelector,omitempty"`
	// LabelName contains the name of the label in the host resource
	LabelName string `json:"labelName"`
	// LabelName contains the value of the label in the host resource
	LabelValue string `json:"labelValue,omitempty"`
	// LabelName contains the limits
	// if Aggregate is false : this limits is the maximum count of host resources
	// if Aggregate is true : this limits is the maximum value of the aggragation of label values
	Quantity int `json:"quantity"`
	// Aggregate indicate if the label value must be aggregated
	Aggregate bool `json:"aggregate,omitempty"`
}

// HostQuotaSpec defines the desired state of HostQuota
type HostQuotaSpec struct {
	// Hard contains a list of Quota
	Hard []Quota `json:"hard,omitempty"`
}

// HostQuotaStatus defines the observed state of HostQuota
type HostQuotaStatus struct {
	// Quota contains the host count for each host type
	Hard []Quota `json:"quotas,omitempty"`
	// Quota contains the host count for each host type
	Used      []Quota `json:"used,omitempty"`
	UsedQuota string  `json:"usedQuota,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:printcolumn:name="REQUEST",type="string",JSONPath=`.status.usedQuota`
// HostQuota is the Schema for the hostquotas API
type HostQuota struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   HostQuotaSpec   `json:"spec,omitempty"`
	Status HostQuotaStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// HostQuotaList contains a list of HostQuota
type HostQuotaList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []HostQuota `json:"items"`
}

func init() {
	SchemeBuilder.Register(&HostQuota{}, &HostQuotaList{})
}
